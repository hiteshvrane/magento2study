<?php
namespace Practice\Training\Block;

class Index extends \Magento\Framework\View\Element\Template
{
   /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setTemplate('Practice_Training::index.phtml');
    }

    public function somefunction() {

        return "some function";
    }
	
}
