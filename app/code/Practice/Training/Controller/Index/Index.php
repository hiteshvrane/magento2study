<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Practice\Training\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Index implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** Result Changing the Handle */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(' This is custom page ');
        return $resultPage;

        /** Load content of PHTML file directly */
/*         $myPHTMLFile = $this->_view->getLayout()->createBlock(
            \Practice\Training\Block\Index::class)->setTemplate('index.phtml')->toHtml();
            
        echo $myPHTMLFile."<br />"; */
        
        /**** content of static block direcly ***********************/
    /*     $blockContent = $this->_view->getLayout->createBlock('Magento\Cms\Block\Block')
                            ->setBlockId('fb')
                            ->toHtml();
        echo $blockContent; */

        /****Load content of static block direcly ***********************/
       /*  $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));

        $block = $resultPage->getLayout()->createBlock('\Practice\Training\Block\Index')
                 ->setTemplate('Practice_Training::index.phtml')
                 ->toHtml();
        $this->getResponse()->setBody($block);      */
			
		/****result Object *********/
		/* $resultPage = $this->resultPageFactory->create();
		$resultPage = $this->addHandle('practice_training_temp');
		$resultPage->getConfig()->getTitle()->set(' Page Heading ');
		
		return $resultPage; */
       // return $this->resultPageFactory->create();
    }
}

