5.4.1.(solution module Unit5_VendorEntity). Create a table with an install script for the module.

• Give the module a setup_version of 0.1.0.
• Create the Setup folder.
• Create the Install Schema class.
• Create a vendor_entity table using DDL methods.
• Execute the installation using the console tool – run php magento setup:upgrade
• Verify that it works by checking the setup_module table.


5.4.2.(solution module Unit4_VendorEntity). Create a regular upgrade script to add an additional column.

• Create the UpgradeSchema class.
• Add an additional column to the vendor_entity table using DDL adapter methods.
• Upgrade the version number in your module.xmlto 0.2.0.
• Run the appropriate console command.
• Verify that it works.



5.4.3.(solution module Unit5_VendorEntity). Create a data upgrade script to set a config value.

• Create the UpgradeDataclass.
• Define a fixture vendor to be installed along with your module.
• Upgrade the module version to 0.2.1.
• Execute the appropriate console command.
• Verify that it works.
