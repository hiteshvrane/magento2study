5.7.5. Create a select attribute with a predefined list of options.

•Create a new customer attribute 'priority' using an upgrade data method.
•Use the frontend_input type 'select'.
•Use the backend_type 'int'.
•Set is_system to 0.
•Assign a custom source model
•Implement the custom attribute source model to list numbers from 1 through 10.
•Test that the attribute works as expected.