5.3.1. (solution module Unit5_ProductSave).
Log every product save operation and specify the product ID and the data that has been changed.

• Create an empty observer class which implements ObserverInterface.
• Create a logger object from \Psr\Log\LoggerInterface in the observer class.
• Implement observer class execute function.
• Create an events.xml file and tie the observer to the “catalog_product_save_after” event.