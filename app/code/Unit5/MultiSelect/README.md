5.7.3. (solution module Unit5_MultiSelect). 
Create a multi select product attribute from an upgrade data method.

• Create a multi select product attribute.
• Set the backend_model property to Magento\Eav\Entity\Attribute\Backend\ArrayBackend
• Add a few options to the attribute.
• Make it visible in the catalog product view page.


5.7.4.(solution module Unit5_MultiSelect).
Customize the rendering of the values from the multiselect product attribute
that you've created in the previous exercise.

• Update attributes in the UpgradeData script, set frontend_model and is_html_allowed_on_front attribute properties.
• Update module setup version.
• Create a frontend model class that renders attribute values as an HTML list rather than as comma-separated values.
• Run appropriate terminal commands to apply the upgrade
• Verify it works on a product view page.