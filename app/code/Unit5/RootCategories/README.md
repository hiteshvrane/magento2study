1.2.1.(module Unit5_RootCategories). Echo the list of all store views and associated root categories.

• Get a list of stores using: Magento\Store\Model\ResourceModel\Store\Collection
• Get root category IDs using:Magento\Store\Model\Store::getRootCategoryId()
• Create a category API data object and load category by ID.
• Add the category name attribute to result array.
• Display stores with the associated root category Names.
