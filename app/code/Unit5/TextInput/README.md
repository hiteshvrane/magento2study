5.7.2.(solution module Unit5_TextInput). Create a text input attribute.

• Create a module. Create an InstallData setup class.
• For adding an attribute, create an instance of the CategorySetup class by calling its factory create method 
and passing setup object as a param. Call its addAttribute method. 
• Apply the setup changes.  
• Open a product in the Admin and confirm that the new attribute exists and can be set on a store view level. 
• Visit a product at the storefront and confirm that the new attribute is visible there.