<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface TrainingRepositoryInterface
{

    /**
     * Save Training
     * @param \TCS\Training\Api\Data\TrainingInterface $training
     * @return \TCS\Training\Api\Data\TrainingInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \TCS\Training\Api\Data\TrainingInterface $training
    );

    /**
     * Retrieve Training
     * @param string $trainingId
     * @return \TCS\Training\Api\Data\TrainingInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($trainingId);

    /**
     * Retrieve Training matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \TCS\Training\Api\Data\TrainingSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Training
     * @param \TCS\Training\Api\Data\TrainingInterface $training
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \TCS\Training\Api\Data\TrainingInterface $training
    );

    /**
     * Delete Training by ID
     * @param string $trainingId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($trainingId);
}

