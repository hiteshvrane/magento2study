<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Api\Data;

interface TrainingSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Training list.
     * @return \TCS\Training\Api\Data\TrainingInterface[]
     */
    public function getItems();

    /**
     * Set training_name list.
     * @param \TCS\Training\Api\Data\TrainingInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

