<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Api\Data;

interface TrainingInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const TRAINING_START_DATE = 'training_start_date';
    const TRAINING_DURATION_HOURS = 'training_duration_hours';
    const TRAINING_COST = 'training_cost';
    const TRAINING_CANDIDATE_NO = 'training_candidate_no';
    const TRAINING_NAME = 'training_name';
    const TRAINING_STATUS = 'training_status';
    const TRAINING_ID = 'training_id';
    const TRAINING_END_DATE = 'training_end_date';

    /**
     * Get training_id
     * @return string|null
     */
    public function getTrainingId();

    /**
     * Set training_id
     * @param string $trainingId
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingId($trainingId);

    /**
     * Get training_name
     * @return string|null
     */
    public function getTrainingName();

    /**
     * Set training_name
     * @param string $trainingName
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingName($trainingName);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \TCS\Training\Api\Data\TrainingExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \TCS\Training\Api\Data\TrainingExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \TCS\Training\Api\Data\TrainingExtensionInterface $extensionAttributes
    );

    /**
     * Get training_duration_hours
     * @return string|null
     */
    public function getTrainingDurationHours();

    /**
     * Set training_duration_hours
     * @param string $trainingDurationHours
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingDurationHours($trainingDurationHours);

    /**
     * Get training_candidate_no
     * @return string|null
     */
    public function getTrainingCandidateNo();

    /**
     * Set training_candidate_no
     * @param string $trainingCandidateNo
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingCandidateNo($trainingCandidateNo);

    /**
     * Get training_cost
     * @return string|null
     */
    public function getTrainingCost();

    /**
     * Set training_cost
     * @param string $trainingCost
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingCost($trainingCost);

    /**
     * Get training_start_date
     * @return string|null
     */
    public function getTrainingStartDate();

    /**
     * Set training_start_date
     * @param string $trainingStartDate
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingStartDate($trainingStartDate);

    /**
     * Get training_end_date
     * @return string|null
     */
    public function getTrainingEndDate();

    /**
     * Set training_end_date
     * @param string $trainingEndDate
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingEndDate($trainingEndDate);

    /**
     * Get training_status
     * @return string|null
     */
    public function getTrainingStatus();

    /**
     * Set training_status
     * @param string $trainingStatus
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingStatus($trainingStatus);
}

