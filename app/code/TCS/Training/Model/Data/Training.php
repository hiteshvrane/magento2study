<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Model\Data;

use TCS\Training\Api\Data\TrainingInterface;

class Training extends \Magento\Framework\Api\AbstractExtensibleObject implements TrainingInterface
{

    /**
     * Get training_id
     * @return string|null
     */
    public function getTrainingId()
    {
        return $this->_get(self::TRAINING_ID);
    }

    /**
     * Set training_id
     * @param string $trainingId
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingId($trainingId)
    {
        return $this->setData(self::TRAINING_ID, $trainingId);
    }

    /**
     * Get training_name
     * @return string|null
     */
    public function getTrainingName()
    {
        return $this->_get(self::TRAINING_NAME);
    }

    /**
     * Set training_name
     * @param string $trainingName
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingName($trainingName)
    {
        return $this->setData(self::TRAINING_NAME, $trainingName);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \TCS\Training\Api\Data\TrainingExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \TCS\Training\Api\Data\TrainingExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \TCS\Training\Api\Data\TrainingExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get training_duration_hours
     * @return string|null
     */
    public function getTrainingDurationHours()
    {
        return $this->_get(self::TRAINING_DURATION_HOURS);
    }

    /**
     * Set training_duration_hours
     * @param string $trainingDurationHours
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingDurationHours($trainingDurationHours)
    {
        return $this->setData(self::TRAINING_DURATION_HOURS, $trainingDurationHours);
    }

    /**
     * Get training_candidate_no
     * @return string|null
     */
    public function getTrainingCandidateNo()
    {
        return $this->_get(self::TRAINING_CANDIDATE_NO);
    }

    /**
     * Set training_candidate_no
     * @param string $trainingCandidateNo
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingCandidateNo($trainingCandidateNo)
    {
        return $this->setData(self::TRAINING_CANDIDATE_NO, $trainingCandidateNo);
    }

    /**
     * Get training_cost
     * @return string|null
     */
    public function getTrainingCost()
    {
        return $this->_get(self::TRAINING_COST);
    }

    /**
     * Set training_cost
     * @param string $trainingCost
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingCost($trainingCost)
    {
        return $this->setData(self::TRAINING_COST, $trainingCost);
    }

    /**
     * Get training_start_date
     * @return string|null
     */
    public function getTrainingStartDate()
    {
        return $this->_get(self::TRAINING_START_DATE);
    }

    /**
     * Set training_start_date
     * @param string $trainingStartDate
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingStartDate($trainingStartDate)
    {
        return $this->setData(self::TRAINING_START_DATE, $trainingStartDate);
    }

    /**
     * Get training_end_date
     * @return string|null
     */
    public function getTrainingEndDate()
    {
        return $this->_get(self::TRAINING_END_DATE);
    }

    /**
     * Set training_end_date
     * @param string $trainingEndDate
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingEndDate($trainingEndDate)
    {
        return $this->setData(self::TRAINING_END_DATE, $trainingEndDate);
    }

    /**
     * Get training_status
     * @return string|null
     */
    public function getTrainingStatus()
    {
        return $this->_get(self::TRAINING_STATUS);
    }

    /**
     * Set training_status
     * @param string $trainingStatus
     * @return \TCS\Training\Api\Data\TrainingInterface
     */
    public function setTrainingStatus($trainingStatus)
    {
        return $this->setData(self::TRAINING_STATUS, $trainingStatus);
    }
}

