<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Model;

use Magento\Framework\Api\DataObjectHelper;
use TCS\Training\Api\Data\TrainingInterface;
use TCS\Training\Api\Data\TrainingInterfaceFactory;

class Training extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'tcs_training_training';
    protected $dataObjectHelper;

    protected $trainingDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param TrainingInterfaceFactory $trainingDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \TCS\Training\Model\ResourceModel\Training $resource
     * @param \TCS\Training\Model\ResourceModel\Training\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        TrainingInterfaceFactory $trainingDataFactory,
        DataObjectHelper $dataObjectHelper,
        \TCS\Training\Model\ResourceModel\Training $resource,
        \TCS\Training\Model\ResourceModel\Training\Collection $resourceCollection,
        array $data = []
    ) {
        $this->trainingDataFactory = $trainingDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve training model with training data
     * @return TrainingInterface
     */
    public function getDataModel()
    {
        $trainingData = $this->getData();
        
        $trainingDataObject = $this->trainingDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $trainingDataObject,
            $trainingData,
            TrainingInterface::class
        );
        
        return $trainingDataObject;
    }
}

