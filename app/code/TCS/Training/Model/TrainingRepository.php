<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TCS\Training\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use TCS\Training\Api\Data\TrainingInterfaceFactory;
use TCS\Training\Api\Data\TrainingSearchResultsInterfaceFactory;
use TCS\Training\Api\TrainingRepositoryInterface;
use TCS\Training\Model\ResourceModel\Training as ResourceTraining;
use TCS\Training\Model\ResourceModel\Training\CollectionFactory as TrainingCollectionFactory;

class TrainingRepository implements TrainingRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $dataTrainingFactory;

    private $storeManager;

    protected $trainingCollectionFactory;

    protected $dataObjectHelper;

    protected $trainingFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceTraining $resource
     * @param TrainingFactory $trainingFactory
     * @param TrainingInterfaceFactory $dataTrainingFactory
     * @param TrainingCollectionFactory $trainingCollectionFactory
     * @param TrainingSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceTraining $resource,
        TrainingFactory $trainingFactory,
        TrainingInterfaceFactory $dataTrainingFactory,
        TrainingCollectionFactory $trainingCollectionFactory,
        TrainingSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->trainingFactory = $trainingFactory;
        $this->trainingCollectionFactory = $trainingCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataTrainingFactory = $dataTrainingFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \TCS\Training\Api\Data\TrainingInterface $training
    ) {
        /* if (empty($training->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $training->setStoreId($storeId);
        } */
        
        $trainingData = $this->extensibleDataObjectConverter->toNestedArray(
            $training,
            [],
            \TCS\Training\Api\Data\TrainingInterface::class
        );
        
        $trainingModel = $this->trainingFactory->create()->setData($trainingData);
        
        try {
            $this->resource->save($trainingModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the training: %1',
                $exception->getMessage()
            ));
        }
        return $trainingModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($trainingId)
    {
        $training = $this->trainingFactory->create();
        $this->resource->load($training, $trainingId);
        if (!$training->getId()) {
            throw new NoSuchEntityException(__('Training with id "%1" does not exist.', $trainingId));
        }
        return $training->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->trainingCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \TCS\Training\Api\Data\TrainingInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \TCS\Training\Api\Data\TrainingInterface $training
    ) {
        try {
            $trainingModel = $this->trainingFactory->create();
            $this->resource->load($trainingModel, $training->getTrainingId());
            $this->resource->delete($trainingModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Training: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($trainingId)
    {
        return $this->delete($this->get($trainingId));
    }
}

