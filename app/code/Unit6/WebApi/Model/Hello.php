<?php
/**
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Unit6\WebApi\Model;

/**
 * Class Hello
 * @package Unit6\WebApi\Model
 */
class Hello implements \Unit6\WebApi\Api\Data\HelloInterface
{
    /**
     * @return string
     */
    public function sayHello() {
        return "HELLO WORLD!";
    }
}