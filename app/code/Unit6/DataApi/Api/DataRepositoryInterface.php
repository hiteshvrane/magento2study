<?php
/**
 *
 * Copyright © Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Unit6\DataApi\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface DataDataApiInterface
 * @package Unit6\DataApi\Api
 */
interface DataRepositoryInterface
{
    /**
     * @return Data\DataSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}