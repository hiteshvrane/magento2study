<?php
namespace Hitesh\CustomRestApi\Api;
/**
 * Interface CustomApiInterface
 *
 * @package Hitesh\CustomRestApi\Api
 */
interface CustomApiInterface
{
    /**
     * Get Customer List
     * @return string
     */
    public function getCustomerList();
}