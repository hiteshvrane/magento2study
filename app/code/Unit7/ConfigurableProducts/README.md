7.3.1. (solution module Unit7_ConfigurableProducts). Configurable ProductsAdd a new filter for configurable products,  
on a products grid page, that filters by number of configurable options.    

• Create  product_listing ui_component.  
• Create data provider class that implements AddFilterToCollectionInterface.  
• Make an option provider class that implements OptionSourceInterface.  
• Don’t forget to add the configurable_options item to the Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider   
addFilterStrategies constructor param in the di.xml file.  
• Checkthatit all works.