7.4.1.(solution module Unit7_RequireVerification). Add a New Column.  
  
• Add new column to the order table require_verification.  
• It should always equal “1” if an order is placed through the checkout.  
• It should equal “0” if it was placed in the Admin.  


7.4.2. (solution module Unit7_RequireVerification).   
Add a require_verification column to the order grid.  


7.4.3. (solution module Unit7_RequireVerification). Mass Action.    
 
• Add a mass action to the orders grid which changes it to “0” for all selected orders.    
• Create a route in the adminhtml area.  
• Create a mass action class.  
• Add mass action to the grid.