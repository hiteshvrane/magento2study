<?php
/**
 *  Copyright © 2019 Magento. All rights reserved.
 *  See COPYING.txt for license details.
 */

namespace Unit7\ComputerGames\Controller\Adminhtml\Game;
use Magento\Backend\App\Action;

/**
 * Class Validate
 * @package Unit7\ComputerGames\Controller\Adminhtml\Game
 */
class Validate extends Action
{
    /**
     * Validator
     */
    public function execute()
    {
        $this->getResponse()->appendBody(json_encode(true));
        $this->getResponse()->sendResponse();
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Unit7_ComputerGames::grid');

    }
}