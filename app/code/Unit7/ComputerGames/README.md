7.4.4.(solution module Unit7_ComputerGames). Create a Table, Grid, and Grid Filters.

• Create a new table, computer_games, with the following fields: 
    game_id, name(varchar), type("rpg", "rts", "mmo", "simulator", "shooter"), trial_period(period in days), release_date.
• For the game table, compose themodel resource model and collection.
• For this table, create a grid with columns that correspond to the fields.
• Make the release_date column optional (not visible by default).
• Create filters that correspond to fields (text for name, dropdown for type, from-to for trial_period, date for release_date).


7.5.1. (solution module Unit7_ComputerGames). Computer entity Create/Edit, New Form features.

• For the computer_games table and grid created in the previous grid section, add a form.
• Use following field types: name(text), type(dropdown: "RPG", "RTS", "MMO", "Simulator", "Shooter"), trial_period(integer), release_date(date)
• Make sure the save/delete/backbuttons are available and functioning.