<?php

namespace Custom\Brand\Controller\Index;

use Magento\Framework\Exception\LocalizedException;

class Post extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $post = $this->getRequest()->getParams();
        $model = $this->_objectManager->create(\Custom\Brand\Model\Brand::class);
        $model->setData($post);
        try {
            $model->save();
            $this->messageManager->addSuccessMessage(__('You saved the Brand.'));
            $this->_objectManager->get('Custom\Brand\Helper\Data')->sendEmail($post);

            return $resultRedirect->setPath('*/*/');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Brand.'));
        }
    }
}
