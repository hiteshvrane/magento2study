<?php


namespace Custom\Brand\Model\Data;

use Custom\Brand\Api\Data\BrandInterface;

class Brand extends \Magento\Framework\Api\AbstractExtensibleObject implements BrandInterface
{

    /**
     * Get brand_id
     * @return string|null
     */
    public function getBrandId()
    {
        return $this->_get(self::BRAND_ID);
    }

    /**
     * Set brand_id
     * @param string $brandId
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

    /**
     * Get brand
     * @return string|null
     */
    public function getBrand()
    {
        return $this->_get(self::BRAND);
    }

    /**
     * Set brand
     * @param string $brand
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function setBrand($brand)
    {
        return $this->setData(self::BRAND, $$brand);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Custom\Brand\Api\Data\BrandExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Custom\Brand\Api\Data\BrandExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Custom\Brand\Api\Data\BrandExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get city
     * @return string|null
     */
    public function getdescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * Set city
     * @param string $city
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function Setdescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

}
