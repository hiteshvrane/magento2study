<?php


namespace Custom\Brand\Model;

use Custom\Brand\Api\Data\BrandInterface;
use Magento\Framework\Api\DataObjectHelper;
use Custom\Brand\Api\Data\BrandInterfaceFactory;

class Brand extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $brandDataFactory;

    protected $_eventPrefix = 'custom_brand_brand';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BrandInterfaceFactory $brandDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Custom\Brand\Model\ResourceModel\Brand $resource
     * @param \Custom\Brand\Model\ResourceModel\Brand\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BrandInterfaceFactory $brandDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Custom\Brand\Model\ResourceModel\Brand $resource,
        \Custom\Brand\Model\ResourceModel\Brand\Collection $resourceCollection,
        array $data = []
    ) {
        $this->brandDataFactory = $brandDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve brand model with brand data
     * @return BrandInterface
     */
    public function getDataModel()
    {
        $brandData = $this->getData();
        
        $brandDataObject = $this->brandDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $brandDataObject,
            $brandData,
            brandInterface::class
        );
        
        return $brandDataObject;
    }
}
