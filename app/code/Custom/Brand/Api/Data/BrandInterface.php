<?php


namespace Custom\Brand\Api\Data;

interface BrandInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const BRAND_ID = 'brand_id';
    const DESCRIPTION = 'description';
    const BRAND = 'brand';

    /**
     * Get brand_id
     * @return string|null
     */
    public function getBrandId();

    /**
     * Set brand_id
     * @param string $brandId
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function setBrandId($brandId);

    /**
     * Get brand
     * @return string|null
     */
    public function getBrand();

    /**
     * Set brand
     * @param string $brand
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function setBrand($brand);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Custom\Brand\Api\Data\BrandExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Custom\Brand\Api\Data\BrandExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Custom\Brand\Api\Data\BrandExtensionInterface $extensionAttributes
    );

    /**
     * Get city
     * @return string|null
     */
    public function getDescription();

    /**
     * Set city
     * @param string $city
     * @return \Custom\Brand\Api\Data\BrandInterface
     */
    public function setDescription($description);

}
