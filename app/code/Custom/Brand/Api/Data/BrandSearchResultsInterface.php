<?php


namespace Custom\Brand\Api\Data;

interface BrandSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Brand list.
     * @return \Custom\Brand\Api\Data\BrandInterface[]
     */
    public function getItems();

    /**
     * Set Brand list.
     * @param \Custom\Brand\Api\Data\BrandInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
