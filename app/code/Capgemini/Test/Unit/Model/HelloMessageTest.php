
<?php

namespace Capgemini\HelloWorld\Test\Unit\Model;

use Capgemini\HelloWorld\Model\HelloMessage;

class HelloMessageTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var HelloMessage
     */
    protected $helloMessage;

    public function setUp() : void
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->helloMessage = $objectManager->getObject('Capgemini\HelloWorld\Model\HelloMessage');
        $this->expectedMessage = 'Hello Magento 2! We will change the world!';
    }

    public function testGetMessage()
    {
        $this->assertEquals($this->expectedMessage, $this->helloMessage->getMessage());
    }
}