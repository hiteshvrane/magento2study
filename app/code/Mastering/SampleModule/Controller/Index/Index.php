<?php

namespace Mastering\SampleModule\Controller\Index;

use Magento\CatalogSearch\Test\Block\Advanced\Result;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
       //$result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
       //$result->setContents('My content');
       // return $result;
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
