1.9.1. (module name: Unit1_CustomConfig)
In the empty module you created in Exercise 1.3.1, add custom configuration xml/xsd files.

To create new xml/xsd files, we have to take the following steps:

Phase 1: Create test.xml and test.xsd files.
Phase 2: Create PHP files to process them: Config, ConfigInterface, Convertor, Reader, SchemaLocator.
Phase 3: Define a preference for ConfigInterface.
Phase 4: Test: In this example we will create a new observer to test this functionality.

