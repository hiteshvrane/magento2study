1.3.1. (module name: Unit1_Test and Unit1_Test2)
Create a new module. Make a mistake in its config. Create a second module dependent on the first.

1.6.1: Dependency Injection

1.	Go into the Magento core modules folder (app\code\Magento or vendor\magento).
       2. Open Catalog module; select 5 different classes from different folders.
       3. What kind of a pattern do you notice?

Pattern: The constructor has a list of objects assigned to protected properties, and then used inside a class. This is what DI
looks like in Magento 2

1.6.2: (module name: Unit1_Test and Unit1_Test2). 
Object Manager.
Go back to the two modules created you created in Exercise 1.3.1, Unit1_Test and Unit1_Test2*.
In Unit1_Test, create the folder “MagentoU”. In this folder, create the class “Test”. The code to be used is given
below.
Note that the word “module” is not usually used in a module name. This is only used here for learning purposes.